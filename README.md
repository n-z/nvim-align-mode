Adds a command `AlignEnter` to vim which toggles on _align mode_.
The first time pressing `<CR>` in _align mode_ will select the current column of the cursor as the column to be aligned to (the `alignment_column`).
Subsequent presses of the `<CR>` will align the current column of the cursor position to the `alignment_column` if the cursor is further left on the time.
Pressing `<Esc>` leaves _alignment mode_.
