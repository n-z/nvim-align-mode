-- Imports the plugin's additional Lua modules.
local align_mode  = require("align-mode.align_mode")

-- Creates an object for the module. All of the module's
-- functions are associated with this object, which is
-- returned when the module is called with `require`.
local M = {}

-- Routes calls made to this module to functions in the
-- plugin's other modules.
-- M.AlignToggle = align_mode.AlignToggle
M.AlignEnter = align_mode.AlignEnter
-- M.AlignExit = align_mode.AlignExit
M.AlignToCursorCol = align_mode.AlignToCursorCol
-- M.SetAlignCol = align_mode.SetAlignCol

return M
