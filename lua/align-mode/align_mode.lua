
local align_mode = false
local align_col = -1

local function align_mode_toggle(state)
  align_mode = state or not align_mode
  if align_mode then
    vim.api.nvim_set_keymap('n', '<CR>', ':lua require("align-mode").SetAlignCol()<CR>', { silent = true, noremap = true })
    vim.api.nvim_set_keymap('n', '<Tab>', ':lua require("align-mode").AlignToCursorCol()<CR>', { silent = true, noremap = true })
    vim.api.nvim_set_keymap('n', '<Esc>', ':lua require("align-mode").AlignExit()<CR>', { silent = true, noremap = true })
  else
    vim.api.nvim_set_keymap('n', '<CR>', '<CR>', { silent = true, noremap = true })
    vim.api.nvim_set_keymap('n', '<Tab>', '<Tab>', { silent = true, noremap = true })
    vim.api.nvim_set_keymap('n', '<Esc>', '<Esc>', { silent = true, noremap = true })
    align_col = -1
  end
end

local function AlignEnter()
  if not align_mode then
    print('Align mode: on')
    align_mode_toggle(true)
  end
end

---@diagnostic disable-next-line: unused-local, unused-function
local function AlignExit()
  if align_mode then
    print('Align mode: off')
    align_mode_toggle(false)
  end
end

---@diagnostic disable-next-line: unused-local, unused-function
local function SetAlignCol()
  align_col = vim.fn.virtcol('.')
end

local function align_to_column(column)
  local line = vim.api.nvim_get_current_line()
  if align_col > column then
    local line1 = string.sub(line, 1, column - 1)
    local line2 = string.sub(line, column)
    local new_line = line1 .. string.rep(' ', align_col - column) .. line2
    vim.api.nvim_set_current_line(new_line)
  end
end

local function AlignToCursorCol()
  local current_col = vim.fn.virtcol('.')
  align_to_column(current_col)
end

return {
  -- AlignToggle = align_mode_toggle,
  AlignEnter = AlignEnter,
  -- AlignExit = AlignExit,
  AlignToCursorCol = AlignToCursorCol,
  -- SetAlignCol = SetAlignCol,
}

