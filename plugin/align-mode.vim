" Title:        Align Mode
" Description:  Add a mode to align text in columns.
" Last Change:  02.07.2023
" Maintainer:   Nikolai Zaki github.com/imlew

" Prevents the plugin from being loaded multiple times. If the loaded
" variable exists, do nothing more. Otherwise, assign the loaded
" variable and continue running this instance of the plugin.
if exists("g:loaded_align_mode")
    finish
endif
let g:loaded_align_mode = 1

" Defines a package path for Lua. This facilitates importing the
" Lua modules from the plugin's dependency directory.
let s:lua_rocks_deps_loc =  expand("<sfile>:h:r") . "/../lua/align-mode/deps"
exe "lua package.path = package.path .. ';" . s:lua_rocks_deps_loc . "/lua-?/init.lua'"

" Exposes the plugin's functions for use as commands in Neovim.
if exists(':Align') == 0
  command! -nargs=0 AlignEnter  lua require("align-mode").AlignEnter()
endif
